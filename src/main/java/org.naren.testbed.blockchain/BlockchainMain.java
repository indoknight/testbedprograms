package org.naren.testbed.blockchain;

public class BlockchainMain {

    public static void main(String[] args){

        System.out.println("In the ProofOfWork method...");

        ProofOfWork pow0 = ProofOfWork.builder().data("We are just poor shepherds").difficulty(0).build();
        ProofOfWork pow1 = ProofOfWork.builder().data("We are just poor shepherds").difficulty(1).build();
        ProofOfWork pow2 = ProofOfWork.builder().data("We are just poor shepherds").difficulty(2).build();
        ProofOfWork pow4 = ProofOfWork.builder().data("We are just poor shepherds").difficulty(4).build();

        System.out.println(pow0.calculate());
        System.out.println();
        System.out.println(pow1.calculate());
        System.out.println();
        System.out.println(pow2.calculate());
        System.out.println();
        System.out.println(pow4.calculate());

    }
}