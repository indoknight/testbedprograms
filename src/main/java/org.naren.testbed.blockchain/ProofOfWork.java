package org.naren.testbed.blockchain;

import lombok.Builder;
import lombok.Getter;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

@Builder
@Getter
public class ProofOfWork {
    private String data;
    private int difficulty;
    private int nonce;

    public String calculate() {
        try {

            String prefix = new String();
            while(difficulty>0){
                prefix += "0";
                difficulty--;
            }

            while (true) {

                MessageDigest md = MessageDigest.getInstance("SHA-256");
                String hash = Base64.getEncoder().encodeToString(md.digest((data + nonce).getBytes()));

                if (hash.startsWith(prefix)) {
                    System.out.println(String.format("Found the hash %s with nonce %d", hash, nonce));
                    return hash;
                }
                nonce++;

            }

        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }

    }

}
