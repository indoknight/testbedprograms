package org.naren.testbed.policyevaluation;

import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.util.Map;
import java.util.Set;

@RequiredArgsConstructor
@Data
public class RoleWithParents {
    private String name;
    private Set<String> parents;
}
