package org.naren.testbed.policyevaluation;

import com.google.common.collect.Sets;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class MultiRoleInRoleResolver {
    private static final String[] roles = {"r1", "r2", "r3", "r4", "r5", "r6"};

    private static final Map<String, Set<String>> assignments;
    static{
        assignments = new HashMap<>();
        assignments.put("r1", Sets.newHashSet("r2", "r3"));
        assignments.put("r2", Sets.newHashSet("r3", "r4", "r5"));
        assignments.put("r3", Sets.newHashSet("r4"));
        assignments.put("r4", Sets.newHashSet("r5"));
        assignments.put("r6", Sets.newHashSet("r4", "r5"));
    }


    public static void main(String... args){
        Map<String, Set<String>> rolesWithParents = new HashMap<>();
        for(String role : roles){
            // Tail recursion
            Set<String> parents = new HashSet<>();
            resolveParents(role, parents, assignments.get(role));
            rolesWithParents.put(role, parents);

        }
        System.out.println(rolesWithParents);
    }

    private static void resolveParents(String root, Set<String> parents, Set<String> currentParents){
        if(currentParents == null){
            return;
        }

        for(String currentParent : currentParents){
            if(currentParent.equals(root)){
                System.err.println("Cyclic dependencies detected between roles "+root+" and "+currentParent);
                continue;
            }
            parents.add(currentParent);
            resolveParents(root, parents, assignments.get(currentParent));
        }
    }

}

