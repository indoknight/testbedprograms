package org.naren.testbed.policyevaluation;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.tuple.Pair;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class RoleInRoleResolver {
    private static final String[] roles = {"r1", "r2", "r3", "r4", "r5"};

    private static final Map<String, String> assignments;
    static{
        assignments = new HashMap<>();
        assignments.put("r1", "r2");
        assignments.put("r2", "r3");
        assignments.put("r3", "r4");
        assignments.put("r4", "r1");
    }


    public static void main(String... args){
        Map<String, Set<String>> rolesWithParents = new HashMap<>();
        for(String role : roles){
           // rolesWithParents.put(role, resolveParents_v2(role, assignments.get(role)));

            // Tail recursion
            Set<String> parents = new HashSet<>();
            resolveParents(role, parents, assignments.get(role));
            rolesWithParents.put(role, parents);

        }
        System.out.println(rolesWithParents);
    }

    private static Set<String> resolveParents_v2(String root, String parent){
        Set<String> parents = new HashSet<>();
        if(parent == null){
            return parents;
        }

        if(parent.equals(root)){
            System.err.println("Cyclic dependencies detected between roles "+root+" and "+parent);
            return parents;
        }
        parents.add(parent);
        parents.addAll(resolveParents_v2(root, assignments.get(parent)));

        return parents;

    }

    private static void resolveParents(String root, Set<String> parents, String currentParent){
        if(currentParent == null){
            return;
        }

        if(currentParent.equals(root)){
            System.err.println("Cyclic dependencies detected between roles "+root+" and "+currentParent);
            return;
        }
        parents.add(currentParent);

        resolveParents(root, parents, assignments.get(currentParent));
    }

}

