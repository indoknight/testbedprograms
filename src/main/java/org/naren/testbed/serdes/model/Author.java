package org.naren.testbed.serdes.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.*;
import org.naren.testbed.serdes.DedupResolver;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonIdentityInfo(
        generator=ObjectIdGenerators.PropertyGenerator.class,
        property="name",
        scope = Author.class,
        resolver = DedupResolver.class
)
public class Author {
    private String name;
}
