package org.naren.testbed.serdes.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Map;
import java.util.Set;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Library {
    public enum Genre{
        HISTORY, SUSPENSE, THRILLER, ACTION;
    }

    // Author to Books
    private Map<String, Set<String>> authorBooks;

    // Country Acted to Actor to Movie to Genre
    private Map<String, Map<String, Map<String, Genre>>> countryToActorToMovies;
}
