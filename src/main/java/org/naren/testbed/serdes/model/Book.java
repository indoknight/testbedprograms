package org.naren.testbed.serdes.model;

import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Book {
    private String name;
    private Author author;
}
