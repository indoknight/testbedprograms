package org.naren.testbed.serdes;

import com.fasterxml.jackson.core.*;
import com.fasterxml.jackson.core.util.DefaultPrettyPrinter;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import org.naren.testbed.serdes.model.Library;
import org.naren.testbed.serdes.model.Library.Genre;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.*;

public class JacksonDeserialiseTest {
    public static void main(String... args) {
        SerdesUtil<Library> serdes = new SerdesUtil<>();

        Library library = getLibrary();
        try {
            // Serialization
            String json0 = serdes.serialize(library);
            System.out.println("Standard JSON Serializaton...");
            System.out.println(json0);
            System.out.println();

            System.out.println("Streaming JSON...");
            String json = serialiseUsingStreaming(library);
            System.out.println(json);
            System.out.println();

            // Deserialization
            Library desLibrary = deserialiseUsingStreaming(json);
            System.out.println("Serialized(deseralized the referenced JSON text)...");
            System.out.println(serdes.serialize(desLibrary));
            System.out.println();

        /*    Map<String, Set<String>> desAuthorBooks = desLibrary.getAuthorBooks();

            if(desAuthorBooks.get("Kathy Sierra") == desAuthorBooks.get("Bert Bates")){
                System.out.println("The two sets have same reference");
            }else{
                System.out.println("The two sets do not have same reference");
            }*/

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static String serialiseUsingStreaming(Library library) throws IOException {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        JsonGenerator jGenerator = new JsonFactory().createGenerator(stream, JsonEncoding.UTF8);
        jGenerator.setPrettyPrinter(new DefaultPrettyPrinter());

        try {
            jGenerator.writeStartObject();

            if (library.getAuthorBooks() != null) {
                // Serialize the author to books map
                jGenerator.writeFieldName("authorBooks");
                jGenerator.writeStartObject();
                Map<String, Set<String>> writtenObjects = new HashMap<>();
                for (Map.Entry<String, Set<String>> entry : library.getAuthorBooks().entrySet()) {
                    jGenerator.writeFieldName(entry.getKey());

                    Optional<Map.Entry<String, Set<String>>> writtenKeyValue =
                            writtenObjects.entrySet().stream().filter(wentry -> entry.getValue().equals(wentry.getValue())).findAny();
                    if (writtenKeyValue.isPresent()) {
                        jGenerator.writeString("@" + writtenKeyValue.get().getKey());
//                        jGenerator.writeStartObject();
//                        jGenerator.writeFieldName("$ref");
//                        jGenerator.writeString("#/authorBooks/Kathy Sierra");
//                        jGenerator.writeEndObject();
                    } else {
                        jGenerator.writeStartArray();
                        for (String book : entry.getValue()) {
                            jGenerator.writeString(book);
                        }
                        jGenerator.writeEndArray();
                        writtenObjects.put(entry.getKey(), entry.getValue());
                    }
                }
                jGenerator.writeEndObject();
            }

            if (library.getCountryToActorToMovies() != null) {
                // Serialise the Country to Actor to Movie Genre map
                jGenerator.writeFieldName("countryToActorToMovies");
                jGenerator.writeStartObject();
                Map<String, Map<String, Map<String, Genre>>> writtenOuterMap = new HashMap<>();
                Map<String, Map<String, Genre>> writtenMiddleMap = new HashMap<>();

                for (Map.Entry<String, Map<String, Map<String, Genre>>> oEntry : library.getCountryToActorToMovies().entrySet()) {

                    Optional<Map.Entry<String, Map<String, Map<String, Genre>>>> outerKeyValue =
                            writtenOuterMap.entrySet().stream().filter(wEntry -> wEntry.getValue().equals(oEntry.getValue())).findAny();
                    if (outerKeyValue.isPresent()) {
                        jGenerator.writeFieldName(oEntry.getKey());
                        jGenerator.writeString("@" + outerKeyValue.get().getKey());
                    } else {
                        if(library.getCountryToActorToMovies().get(oEntry.getKey()) != null){
                            jGenerator.writeFieldName(oEntry.getKey());
                            jGenerator.writeStartObject();
                            for (Map.Entry<String, Map<String, Genre>> mEntry : library.getCountryToActorToMovies().get(oEntry.getKey()).entrySet()) {
                                Optional<Map.Entry<String, Map<String, Genre>>> middleKeyValue =
                                        writtenMiddleMap.entrySet().stream().filter(wEntry -> wEntry.getValue().equals(mEntry.getValue())).findAny();
                                if (middleKeyValue.isPresent()) {
                                    jGenerator.writeFieldName(mEntry.getKey());
                                    jGenerator.writeString("@" + middleKeyValue.get().getKey());
                                } else {
                                    if(library.getCountryToActorToMovies().get(oEntry.getKey()).get(mEntry.getKey()) != null){
                                        jGenerator.writeFieldName(mEntry.getKey());
                                        jGenerator.writeStartObject();
                                        for (Map.Entry<String, Genre> iEntry : library.getCountryToActorToMovies().get(oEntry.getKey()).get(mEntry.getKey()).entrySet()) {
                                            jGenerator.writeFieldName(iEntry.getKey());
                                            jGenerator.writeString(iEntry.getValue().toString());
                                        }
                                        writtenMiddleMap.put(oEntry.getKey() + "." + mEntry.getKey(), mEntry.getValue());
                                        jGenerator.writeEndObject();
                                    }else{
                                        jGenerator.writeFieldName(mEntry.getKey());
                                        jGenerator.writeNull();
                                    }
                                }
                            }
                            writtenOuterMap.put(oEntry.getKey(), oEntry.getValue());
                            jGenerator.writeEndObject();
                        }else{
                            jGenerator.writeFieldName(oEntry.getKey());
                            jGenerator.writeNull();
                        }
                    }
                }
                jGenerator.writeEndObject();
            }
            jGenerator.writeEndObject();
        }finally {
            jGenerator.close();
        }
        return new String(stream.toByteArray(), "UTF-8");
    }

    private static Library deserialiseUsingStreaming(String json) {
        Library library = new Library();
        try {
            JsonParser jParser = new JsonFactory().createParser(json);

            while (jParser.nextToken() != JsonToken.END_OBJECT) {
                if ("authorBooks".equals(jParser.getCurrentName())) {
                    jParser.nextToken();
                    Map<String, Set<String>> authorBooks = new HashMap<>();
                    while (jParser.nextToken() != JsonToken.END_OBJECT) {
                        String author = jParser.getCurrentName();
                        Set<String> books = new HashSet<>();
                        jParser.nextToken();
                        if (jParser.getText().startsWith("@")) {
                            String existingRefKey = jParser.getText().substring(1);
                            if (authorBooks.containsKey(existingRefKey)) {
                                books = authorBooks.get(existingRefKey);
                            }
                        } else {
                            while (jParser.nextToken() != JsonToken.END_ARRAY) {
                                books.add(jParser.getText());
                            }
                        }
                        authorBooks.put(author, books);
                    }
                    library.setAuthorBooks(authorBooks);
                } else if ("countryToActorToMovies".equals(jParser.getCurrentName())) {
                    jParser.nextToken();
                    Map<String, Map<String, Map<String, Genre>>> countryToActorToMoviesRef = new HashMap<>();
                    Map<String, Map<String, Genre>> actorToMoviesRef = new HashMap<>();
                    // Movies -> Genre reference object is not required as Genre is immutable

                    Map<String, Map<String, Map<String, Genre>>> countryToActorToMovies = new HashMap<>();
                    while (jParser.nextToken() != JsonToken.END_OBJECT) {
                        String country = jParser.getCurrentName();
                        Map<String, Map<String, Genre>> actorToMovies = null;
                        jParser.nextToken();
                        if(jParser.getCurrentToken() != JsonToken.VALUE_NULL){
                            if (jParser.getText().startsWith("@")) {
                                String existingCountryRefKey = jParser.getText().substring(1);
                                if (countryToActorToMoviesRef.containsKey(existingCountryRefKey)) {
                                    actorToMovies = countryToActorToMoviesRef.get(existingCountryRefKey);
                                }
                            } else {
                                actorToMovies = new HashMap<>();
                                while (jParser.nextToken() != JsonToken.END_OBJECT) {
                                    String actor = jParser.getCurrentName();
                                    Map<String, Genre> movieGenres = null;
                                    jParser.nextToken();
                                    if(jParser.getCurrentToken() != JsonToken.VALUE_NULL){
                                        if (jParser.getText().startsWith("@")) {
                                            String existingActorRefKey = jParser.getText().substring(1);
                                            if (actorToMoviesRef.containsKey(existingActorRefKey)) {
                                                movieGenres = actorToMoviesRef.get(existingActorRefKey);
                                            }
                                        } else {
                                            movieGenres = new HashMap<>();
                                            while (jParser.nextToken() != JsonToken.END_OBJECT) {
                                                String movie = jParser.getText();
                                                jParser.nextToken();
                                                String genre = jParser.getText();
                                                movieGenres.put(movie, Genre.valueOf(genre));
                                            }
                                            actorToMoviesRef.put(country+"."+actor, movieGenres);
                                        }
                                    }
                                    actorToMovies.put(actor, movieGenres);
                                }
                                countryToActorToMoviesRef.put(country, actorToMovies);
                            }
                        }
                        countryToActorToMovies.put(country, actorToMovies);
                    }
                    library.setCountryToActorToMovies(countryToActorToMovies);
                }
            }
            jParser.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return library;
    }

    private static Library getLibrary() {
        // Set author -> books map
        String author1 = new String("J.K.Rowling");
        String author2 = new String("Kathy Sierra");
        String author3 = new String("Bert Bates");

        String book1 = new String("Harry Potter and the Philosopher's Stone");
        String book2 = new String("Harry Potter and the Chamber of Secrets");
        String book3 = new String("Harry Potter and the Goblet of Fire");


        String book4 = new String("Head First Java");
        String book5 = new String("Head First EJB");
        String book6 = new String("Head First Servlets");

        Set<String> mythicalBooks = ImmutableSet.of(book1, book2, book3);
        Set<String> techBooks = ImmutableSet.of(book4, book5, book6);

        Map<String, Set<String>> authorBooks = ImmutableMap.of(author1, mythicalBooks, author2, techBooks, author3, techBooks);

        // Set Country -> Actor -> Movie -> Genre

        Map<String, Genre> jmGlobalMovieGenres = ImmutableMap.of("Aquaman", Genre.ACTION, "Road to Paloma", Genre.THRILLER, "Justice League", Genre.ACTION);

        Map<String, Genre> jmUSMovieGenres = ImmutableMap.of("Aquaman", Genre.ACTION, "Justice League", Genre.ACTION);
        Map<String, Genre> jmUKMovieGenres = ImmutableMap.of("Justice League", Genre.ACTION);


        Map<String, Genre> baGlobalMovieGenres = ImmutableMap.of("Batman", Genre.ACTION, "Justice League", Genre.THRILLER);

        Map<String, Genre> baUSMovieGenres = ImmutableMap.of("Batman", Genre.ACTION);
        Map<String, Genre> baUKMovieGenres = ImmutableMap.of("Justice League", Genre.THRILLER);

        Map<String, Genre> mfGlobalMovieGenres = ImmutableMap.of("Angel Has Fallen", Genre.THRILLER, "London Has Fallen", Genre.THRILLER);
        Map<String, Genre> mfUSMovieGenres = ImmutableMap.of("Angel Has Fallen", Genre.THRILLER);
        Map<String, Genre> mfUKMovieGenres = ImmutableMap.of("London Has Fallen", Genre.THRILLER);
        Map<String, Genre> mfCanadaMovieGenres = ImmutableMap.of("London Has Fallen", Genre.THRILLER);

        // Global Actor Movies
        Map<String, Map<String, Genre>> globalActorToMovies = ImmutableMap.of("Jason Momoa", jmGlobalMovieGenres,
                "Ben Affleck", baGlobalMovieGenres,
                "Morgan Freeman", mfGlobalMovieGenres,
                "Gerald Butler", mfGlobalMovieGenres);

        // US Actor Movies
        Map<String, Map<String, Genre>> usActorToMovies = ImmutableMap.of("Jason Momoa", jmUSMovieGenres,
                "Ben Affleck", baUSMovieGenres, "Morgan Freeman", mfUSMovieGenres);

        // UK Actor Movies
        Map<String, Map<String, Genre>> ukActorToMovies = ImmutableMap.of("Jason Momoa", jmGlobalMovieGenres,
                "Ben Affleck", baUKMovieGenres, "Morgan Freeman", mfUKMovieGenres);

        // Northern Ireland Actor Movies
        Map<String, Map<String, Genre>> niActorToMovies = ukActorToMovies;

        // Ireland Actor Movies
//        Map<String, Map<String, Genre>> irActorToMovies = ImmutableMap.of("Morgan Freeman", mfUKMovieGenres,
//                "Gerald Butler", mfUKMovieGenres);
        Map<String, Map<String, Genre>> irActorToMovies = new HashMap<>();
        irActorToMovies.put("Morgan Freeman", mfUKMovieGenres);
        irActorToMovies.put("Gerald Butler", null);

        // Canada Actor Movies
        Map<String, Map<String, Genre>> canadaActorToMovies = ImmutableMap.of("Jason Momoa", jmUKMovieGenres,
                "Ben Affleck", baUKMovieGenres, "Morgan Freeman", mfUKMovieGenres, "Gerald Butler", mfCanadaMovieGenres);


//        Map<String, Map<String, Map<String, Genre>>> countryToActorToMovies = ImmutableMap.of(
//                "Global", globalActorToMovies,
//                "US", usActorToMovies,
//                "UK", ukActorToMovies,
//                "Ireland", irActorToMovies,
//                "Northern Ireland", niActorToMovies
//              // "Canada", canadaActorToMovies
//        );

        Map<String, Map<String, Map<String, Genre>>> countryToActorToMovies = new HashMap<>();
         countryToActorToMovies.put("Global", globalActorToMovies);
         countryToActorToMovies.put("US", null);
         countryToActorToMovies.put("UK", ukActorToMovies);
        countryToActorToMovies.put("Ireland", irActorToMovies);
        countryToActorToMovies.put("Northern Ireland", niActorToMovies);


      //  Library library = new Library(authorBooks, countryToActorToMovies);
       //   Library library = new Library(authorBooks, null);
          Library library = new Library(null, countryToActorToMovies);
        return library;
    }


  /*  private static <T> void serializeMapConstruct(JsonGenerator jGenerator, Map<String, Object> map) throws Exception{

        if(map.get(key) instanceof Map){
            Map<String, Object> innerMap = (Map) map.get(key);


            for(Map.Entry<String, T> mEntry : map.get(key).entrySet()){
                jGenerator.writeFieldName(mEntry.getKey());

                Optional<Map.Entry<String, Map<String, Genre>>> middleKeyValue =
                        writtenMiddleMap.entrySet().stream().filter(wentry -> mEntry.getValue().equals(wentry.getValue())).findAny();
                if(middleKeyValue.isPresent()){
                    jGenerator.writeString("@"+middleKeyValue.get().getKey());
                }else{
//                    jGenerator.writeStartObject();
//                    for(Map.Entry<String, Genre> iEntry : library.getCountryToActorToMovies().get(oEntry.getKey()).get(mEntry.getKey()).entrySet()){
//                        jGenerator.writeFieldName(iEntry.getKey());
//                        jGenerator.writeString(iEntry.getValue().toString());
//                    }
//                    jGenerator.writeEndObject();
                    serializeMapConstruct(jGenerator, innerMap.get)
                    writtenMiddleMap.put(mEntry.getKey(), mEntry.getValue());
                }
            }

        }else if(map.get(key) instanceof Set){
            Set<String> innerSet = (Set) map.get(key);
        }else if(map.get(key) instanceof Genre){
            // For any other types end the recursion
            Map<String, Genre> genreMap = (Map) map;

            jGenerator.writeStartObject();
            for(Map.Entry<String, Genre> iEntry : genreMap.entrySet()){
                jGenerator.writeFieldName(iEntry.getKey());
                jGenerator.writeString(iEntry.getValue().toString());
            }
            jGenerator.writeEndObject();
        }else{
            throw new UnsupportedOperationException("The nested tail-end map value is not supported");
        }
    }*/


}
