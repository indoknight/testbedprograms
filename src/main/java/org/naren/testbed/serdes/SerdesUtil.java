package org.naren.testbed.serdes;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

public class SerdesUtil<T> {

    private ObjectMapper mapper = new ObjectMapper();

    public SerdesUtil(){
        mapper.enable(SerializationFeature.INDENT_OUTPUT);
    }

    public String serialize(T t){
        try{
            return mapper.writeValueAsString(t);
        }catch(JsonProcessingException e){
            throw new RuntimeException(e);
        }
    }

    public T deserialize(String json, TypeReference<T> t){
        try{
            return mapper.readValue(json, t);
        }catch(JsonProcessingException e){
            throw new RuntimeException(e);
        }
    }

}

