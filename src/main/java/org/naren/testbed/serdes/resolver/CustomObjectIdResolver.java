package org.naren.testbed.serdes.resolver;

import com.fasterxml.jackson.annotation.ObjectIdGenerator;
import com.fasterxml.jackson.annotation.ObjectIdResolver;

import java.util.HashMap;
import java.util.Map;

public class CustomObjectIdResolver implements ObjectIdResolver {
    protected Map<ObjectIdGenerator.IdKey, Object> _items;

    public CustomObjectIdResolver() {
    }

    @Override
    public void bindItem(ObjectIdGenerator.IdKey id, Object ob) {
        if (this._items == null) {
            this._items = new HashMap();
        } else {
            Object old = this._items.get(id);
            if (old != null) {
                if (old == ob) {
                    return;
                }

                throw new IllegalStateException("Already had POJO for id (" + id.key.getClass().getName() + ") [" + id + "]");
            }
        }

        this._items.put(id, ob);
         System.out.println(this._items);
    }

    @Override
    public Object resolveId(ObjectIdGenerator.IdKey id) {
        return this._items == null ? null : this._items.get(id);
    }

    @Override
    public boolean canUseFor(ObjectIdResolver resolverType) {
        return resolverType.getClass() == this.getClass();
    }

    @Override
    public ObjectIdResolver newForDeserialization(Object context) {
        return new CustomObjectIdResolver();
    }
}
